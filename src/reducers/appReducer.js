import {
    APP_SET_TEXT,
    APP_SET_TITLE
} from '../constants/appConstants';

const initialState = {
    text: '',
    title: ''
};


export default function appStore(state = initialState, action) {
    switch (action.type) {
        case APP_SET_TEXT:
            return { ...state, text: action.payload };

        case APP_SET_TITLE:
            return { ...state, title: action.payload };

        default:
            return state;
    }
}
