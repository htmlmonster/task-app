import { ADD_TASK_GROUP, SELECT_TASK_GROUP, DELETE_TASK_GROUP } from '../constants/taskListSidebarConstants';

const initialState = {
    taskGroupList: [],
    currentGroup: null
};


export default function taskListSidebarStore(state = initialState, action) {
    switch (action.type) {
        case ADD_TASK_GROUP:
            return { ...state, taskGroupList: [...state.taskGroupList, action.payload] };

        case SELECT_TASK_GROUP:
            return { ...state, currentGroup: action.payload };

        case DELETE_TASK_GROUP:
            const groupList = state.taskGroupList.filter(group => group.id !== action.payload);

            return { ...state, taskGroupList: groupList, currentGroup: null };

        default:
            return state;
    }
}
