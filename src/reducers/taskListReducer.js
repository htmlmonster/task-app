import { ADD_TASK, DELETE_TASK } from '../constants/taskListConstants';

const initialState = {
    taskList: []
};

export default function taskListStore(state = initialState, action) {
    switch (action.type) {
        case ADD_TASK:
            return { ...state, taskList: [action.payload, ...state.taskList] };

        case DELETE_TASK:
            const newTaskList = state.taskList.filter(task => task.id !== action.payload);

            return { ...state, taskList: newTaskList };

        default:
            return state;
    }
}
