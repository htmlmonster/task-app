import { combineReducers } from 'redux';

import taskListSidebarReducer from '../reducers/taskListSidebarReducer';
import taskListReducer from '../reducers/taskListReducer';

const common = {
    taskListSidebarStore: taskListSidebarReducer,
    taskListStore: taskListReducer
};

export default combineReducers(common);
