import {
    APP_SET_TEXT
} from '../constants/appConstants';

export const setText = text => dispatch => {
    dispatch({
        type: APP_SET_TEXT,
        payload: text
    });
};
