import { ADD_TASK, DELETE_TASK } from '../constants/taskListConstants';

export const addTask = task => dispatch => {
    dispatch({
        type: ADD_TASK,
        payload: task
    });
};

export const deleteTask = taskId => dispatch => {
    dispatch({
        type: DELETE_TASK,
        payload: taskId
    });
};
