import { ADD_TASK_GROUP, SELECT_TASK_GROUP, DELETE_TASK_GROUP } from '../constants/taskListSidebarConstants';

export const addTaskGroup = taskGroup => dispatch => {
    dispatch({
        type: ADD_TASK_GROUP,
        payload: taskGroup
    });
};

export const selectTaskGroup = currentGroup => dispatch => {
    dispatch({
        type: SELECT_TASK_GROUP,
        payload: currentGroup
    });
};

export const deleteTaskGroup = groupId => dispatch => {
    dispatch({
        type: DELETE_TASK_GROUP,
        payload: groupId
    });
};
