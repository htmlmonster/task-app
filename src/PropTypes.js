import PropTypes from 'prop-types';

export const matchPropType = PropTypes.shape({
    params: PropTypes.shape({
        id: PropTypes.string
    })
});

export const taskPropType = PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string
});

export const tasksPropType = PropTypes.arrayOf(
    PropTypes.shape({
        id: PropTypes.string,
        text: PropTypes.string,
        note: PropTypes.string,
        due: PropTypes.instanceOf(Date)
    })
);
