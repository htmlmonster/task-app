import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import configureStore from './store/configureStore';

import HomePageLayout from './components/pages/homePage/HomePageLayout.jsx';

import './styles/main.scss';

const store = configureStore();

render(
    <Provider store={store}>
        <MuiThemeProvider>
            <HomePageLayout />
        </MuiThemeProvider>
    </Provider>,
    document.querySelector('#mount-point'),
);
