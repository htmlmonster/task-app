import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { taskPropType } from '../PropTypes';

// Actions
import { addTaskGroup, selectTaskGroup } from '../actions/taskListSidebarActions';

// Components
import TaskListSidebar from '../components/pages/homePage/taskListSidebar/TaskListSidebar.jsx';

class TaskListSidebarContainer extends Component {
    render() {
        const { taskGroupList, addTaskGroup, selectTaskGroup } = this.props;

        return <TaskListSidebar
            taskGroupList={taskGroupList}
            addTaskGroup={addTaskGroup}
            selectTaskGroup={selectTaskGroup}
        />;
    }
}

TaskListSidebarContainer.propTypes = {
    taskGroupList: PropTypes.arrayOf(taskPropType),
    addTaskGroup: PropTypes.func.isRequired,
    selectTaskGroup: PropTypes.func.isRequired
};

export default connect(
    store => ({
        taskGroupList: store.taskListSidebarStore.taskGroupList
    }),
    dispatch => ({
        ...bindActionCreators({ addTaskGroup, selectTaskGroup }, dispatch)
    })
)(TaskListSidebarContainer);
