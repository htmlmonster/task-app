import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { taskPropType, tasksPropType } from '../PropTypes';

// Actions
import { deleteTaskGroup } from '../actions/taskListSidebarActions';
import { addTask, deleteTask } from '../actions/taskListActions';

// Components
import TaskList from '../components/pages/homePage/taskList/TaskList.jsx';

class TaskListContainer extends Component {
    render() {
        const { currentGroup, deleteTaskGroup, taskList, addTask, deleteTask } = this.props;

        return this.props.currentGroup && <TaskList
            currentGroup={currentGroup}
            deleteTaskGroup={deleteTaskGroup}
            addTask={addTask}
            taskList={taskList}
            deleteTask={deleteTask}
        />;
    }
}

TaskListContainer.propTypes = {
    taskList: tasksPropType,
    currentGroup: taskPropType,
    addTask: PropTypes.func,
    deleteTask: PropTypes.func,
    deleteTaskGroup: PropTypes.func
};

export default connect(
    store => ({
        currentGroup: store.taskListSidebarStore.currentGroup,
        taskList: store.taskListStore.taskList
    }),
    dispatch => ({
        ...bindActionCreators({ deleteTaskGroup, addTask, deleteTask }, dispatch)
    })
)(TaskListContainer);
