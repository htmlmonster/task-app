import React, { Component } from 'react';
import propTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import * as appActions from '../actions/appActions';

import './App.scss';

class App extends Component {
    constructor() {
        super();

        this.state = {
            text: ''
        };
    }

    handleOnChange = event => {
        this.setState({
            text: event.target.value
        });
    };

    handleOnSubmit = event => {
        event.preventDefault();

        this.props.setText(this.state.text);
        this.setState({
            text: ''
        });
    };

    render() {
        return <div className='wrapper'>
            <form onSubmit={this.handleOnSubmit}>
                <TextField value={this.state.text} onChange={this.handleOnChange} />
                <RaisedButton onClick={this.handleOnSubmit}>Submit</RaisedButton>
                <div>{this.props.text}</div>
            </form>
        </div>;
    }
}

App.defaultProps = {
    text: ''
};

App.propTypes = {
    text: propTypes.string,
    setText: propTypes.func.isRequired
};

export default connect(
    state => ({
        text: state.appStore.text
    }),
    dispatch => ({
        ...bindActionCreators(appActions, dispatch)
    })
)(App);

