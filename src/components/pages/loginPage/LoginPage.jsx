import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

import './LoginPage.scss';

const LoginPage = () => (
    <div className='wrapper'>
        <div className='container'>
            <div className='login-page'>
                <h1>Almost Google tasks</h1>
                <p>Organise your life!</p>
                <RaisedButton className='login-btn' label={'LOG IN WITH GOOGLE'} />
            </div>
            <div>
                <img
                    src='img/desk.png'
                    alt='desk'
                />
            </div>
        </div>
    </div>
);

export default LoginPage;
