import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'material-ui/Checkbox';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Divider from 'material-ui/Divider';
import moment from 'moment';

import './Task.scss';

class Task extends Component {
    handleOnDelete = taskId => () => {
        this.props.deleteTask(taskId);
    };

    render() {
        return (
            <div className='Task__wrapper'>
                <div className='Task'>
                    <Checkbox
                        className='Task__checkbox'
                    />
                    <div className='Task__text'>
                        <div className='Task__title'>
                            {this.props.text}
                            {
                                this.props.note ?
                                    <span className='Task__note'>
                                        {this.props.note}
                                    </span> :
                                    null
                            }
                        </div>
                        {
                            this.props.due ?
                                <div className='Task__due'>
                                    {`due ${moment(this.props.due).fromNow()}`}
                                </div> :
                                null
                        }
                    </div>
                    <IconMenu
                        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                        anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                    >
                        <MenuItem primaryText='Edit' />
                        <MenuItem primaryText='Delete' onClick={this.handleOnDelete(this.props.id)} />
                    </IconMenu>
                </div>
                <Divider />
            </div>
        );
    }
}

Task.propTypes = {
    text: PropTypes.string.isRequired,
    note: PropTypes.string,
    due: PropTypes.instanceOf(Date),
    id: PropTypes.number.isRequired,
    deleteTask: PropTypes.func.isRequired
};

export default Task;
