import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';

import { taskPropType, tasksPropType } from '../../../../PropTypes';
import './TaskList.scss';
import Task from './task/Task.jsx';
import PopupAddTask from './popupAddTask/PopupAddTask.jsx';

class TaskList extends Component {
    constructor() {
        super();

        this.state = {
            isOpen: false
        };
    }

    handleDeleteTaskGroup = groupId => () => {
        event.preventDefault();

        const permission = confirm('Delete this task group?');

        if (permission) {
            this.props.deleteTaskGroup(groupId);
        }
    };

    handleShowAddTaskPopup = () => {
        event.preventDefault();

        this.setState({ isOpen: true });
    };

    hideAddTaskPopup = () => {
        this.setState({ isOpen: false });
    };

    render() {
        return (
            <div className='task-list-wrapper'>
                <div className='task-list-topbar'>

                    <div className='task-list-bar'>
                        <div className='list-name'>{this.props.currentGroup.title}</div>

                        <div className='btns-container'>
                            <IconButton
                                className='task-list-btn'
                                onClick={this.handleShowAddTaskPopup}
                            >
                                {<i className='material-icons'>add</i>}
                            </IconButton>
                            <IconButton
                                className='task-list-btn'
                                onClick={this.handleDeleteTaskGroup(this.props.currentGroup.id)}
                            >
                                <i className='material-icons'>delete</i>
                            </IconButton>
                        </div>

                    </div>

                </div>
                <div>
                    {
                        this.props.taskList.map(item => (
                            <Task
                                id={item.id}
                                key={item.id}
                                text={item.text}
                                note={item.note}
                                due={item.due}
                                deleteTask={this.props.deleteTask}
                            />
                        ))
                    }
                </div>
                {
                    this.state.isOpen && <PopupAddTask
                        hideAddTaskPopup={this.hideAddTaskPopup}
                        handleOnSubmit={this.props.addTask}
                    />
                }
            </div>
        );
    }
}

TaskList.propTypes = {
    taskList: tasksPropType,
    currentGroup: taskPropType,
    deleteTask: PropTypes.func.isRequired,
    addTask: PropTypes.func.isRequired,
    deleteTaskGroup: PropTypes.func.isRequired
};

export default TaskList;
