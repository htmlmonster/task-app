import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

import './PopupAddTask.scss';

class PopupAddTask extends Component {
    constructor() {
        super();

        this.state = {
            text: '',
            note: '',
            due: null
        };
    }

    handleOnChangeText = event => {
        this.setState({ text: event.target.value });
    };

    handleOnChangeNote = event => {
        this.setState({ note: event.target.value });
    };

    handleOnChangeTime = (event, date) => {
        this.setState({ due: date });
    };

    handleOnSubmit = event => {
        event.preventDefault();

        this.props.handleOnSubmit({
            id: (new Date()).valueOf(),
            text: this.state.text,
            note: this.state.note,
            due: this.state.due
        });
        this.handleCancel();
    };

    handleCancel = () => {
        event.preventDefault();

        this.props.hideAddTaskPopup();
        this.setState({
            text: '',
            note: '',
            due: null
        });
    };

    render() {
        return (
            <div className='addTaskPopup'>
                <div className='addTaskPopup__wrapper' />
                <form>
                    <Paper className='addTaskPopup__form' zDepth={5}>
                        <h2 className='addTaskPopup__title'>Add task</h2>
                        <TextField
                            fullWidth
                            hintText='Enter task title'
                            value={this.state.text}
                            onChange={this.handleOnChangeText}
                        />
                        <TextField
                            fullWidth
                            hintText='Enter task note'
                            value={this.state.note}
                            onChange={this.handleOnChangeNote}
                        />
                        <DatePicker
                            autoOk
                            fullWidth
                            floatingLabelText='Enter due time'
                            value={this.state.due}
                            onChange={this.handleOnChangeTime}
                        />
                        <div className='addTaskPopup__buttonContainer'>
                            <FlatButton
                                className='addTaskPopup__btnSubmit'
                                label='Submit'
                                onClick={this.handleOnSubmit}
                                disabled={!this.state.text}
                            />
                            <FlatButton
                                className='addTaskPopup__btnCancel'
                                label='Cancel'
                                onClick={this.handleCancel}
                            />
                        </div>
                    </Paper>
                </form>
            </div>
        );
    }
}

PopupAddTask.propTypes = {
    hideAddTaskPopup: PropTypes.func,
    handleOnSubmit: PropTypes.func
};

export default PopupAddTask;
