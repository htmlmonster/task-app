import React from 'react';

import TaskListSidebarContainer from '../../../containers/TaskListSidebarContainer.jsx';
import TaskListContainer from '../../../containers/TaskListContainer.jsx';

import './HomePageLayout.scss';

const HomePageLayout = () => (
    <div className='layoutWrapper'>
        <div className='pages-wrapper'>
            <TaskListSidebarContainer />
            <TaskListContainer />
        </div>
    </div>
);

export default HomePageLayout;
