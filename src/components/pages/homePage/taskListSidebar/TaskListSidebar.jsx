import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import PopupAddTaskList from './popupAddTaskList/PopupAddTaskList.jsx';

import './TaskListSidebar.scss';

class TaskListSidebar extends Component {
    constructor() {
        super();

        this.state = {
            isOpen: false
        };
    }

    handleShowAddTaskListPopup = () => {
        this.setState({ isOpen: true }, () => {
            this.input && this.input.focus();
        });
    };

    handleHideAddTaskListPopup = () => {
        this.setState({ isOpen: false });
    };

    handleSelectGroup = currentGroup => () => {
        event.preventDefault();

        this.props.selectTaskGroup(currentGroup);
    };

    setRef = fieldInput => {
        if (fieldInput) {
            this.input = fieldInput.input;
        }
    };

    render() {
        return (
            <div className='home-wrapper'>
                <Paper className='home-paper'>
                    <Menu>
                        <h1 className='home-title'>Almost Google Tasks</h1>
                        <Divider />
                        <MenuItem primaryText='Home' leftIcon={<i className='material-icons'>home</i>} />
                        <MenuItem primaryText='About' leftIcon={<i className='material-icons'>view_list</i>} />
                        <Divider />
                        <strong className='lists'>Task Lists</strong>
                        {
                            this.props.taskGroupList.map(({ id, title }) => (
                                <MenuItem
                                    key={id}
                                    primaryText={title}
                                    leftIcon={<i className='material-icons'>folder</i>}
                                    onClick={this.handleSelectGroup({ id, title })}
                                />
                            ))
                        }

                        <MenuItem
                            primaryText='Create new list'
                            leftIcon={<i className='material-icons'>add</i>}
                            onClick={this.handleShowAddTaskListPopup}
                        />
                        <Divider />
                        <MenuItem primaryText='Log out' leftIcon={<i className='material-icons'>exit_to_app</i>} />
                        {
                            this.state.isOpen
                            && <PopupAddTaskList
                                handleOnSubmit={this.props.addTaskGroup}
                                handleOnHideAddTaskListPopup={this.handleHideAddTaskListPopup}
                                setRef={this.setRef}
                            />
                        }
                    </Menu>
                </Paper>
            </div>
        );
    }
}

TaskListSidebar.defaultProps = {
    taskGroupList: []
};

TaskListSidebar.propTypes = {
    taskGroupList: PropTypes.arrayOf({
        id: PropTypes.string,
        title: PropTypes.string
    }),
    selectTaskGroup: PropTypes.func,
    addTaskGroup: PropTypes.func
};

export default TaskListSidebar;
