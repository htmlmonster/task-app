import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

import './PopupAddTaskList.scss';

export default class PopupAddTaskList extends Component {
    constructor() {
        super();

        this.state = {
            title: ''
        };
    }

    handleOnChange = event => {
        this.setState({ title: event.target.value });
    };

    handleOnSubmit = () => {
        event.preventDefault();

        this.props.handleOnSubmit({
            id: (new Date).valueOf(),
            title: this.state.title
        });
        this.handleCancel();
    };

    handleCancel = () => {
        event.preventDefault();

        this.props.handleOnHideAddTaskListPopup();
        this.setState({ title: '' });
    };

    render() {
        return (
            <div className='addTaskListPopup'>
                <div className='addTaskListPopup__wrapper' onClick={this.handleCancel} />
                <form onSubmit={this.handleOnSubmit}>
                    <Paper className='addTaskListPopup__form' zDepth={5}>
                        <h2 className='addTaskListPopup__title'>Add task list</h2>
                        <TextField
                            ref={this.props.setRef}
                            fullWidth
                            hintText='Enter task list name'
                            value={this.state.title}
                            onChange={this.handleOnChange}
                        />
                        <div className='addTaskListPopup__buttonContainer'>
                            <FlatButton
                                className='addTaskListPopup__btnSubmit'
                                label='Submit'
                                onClick={this.handleOnSubmit}
                                disabled={!this.state.title}
                            />
                            <FlatButton
                                className='addTaskListPopup__btnCancel'
                                label='Cancel'
                                onClick={this.handleCancel}
                            />
                        </div>
                    </Paper>
                </form>
            </div>
        );
    }
}

PopupAddTaskList.propTypes = {
    setRef: PropTypes.func,
    handleOnHideAddTaskListPopup: PropTypes.func,
    handleOnSubmit: PropTypes.func
};
