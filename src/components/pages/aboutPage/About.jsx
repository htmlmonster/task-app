import React from 'react';
import Paper from 'material-ui/Paper';

import './About.scss';

const About = () => (
    <div className='container'>
        <Paper className='paper' zDepth={1}>
            <h2>Almost Google Tasks</h2>
            <p>This application is written based on <a href=''>Google Tasks API</a> using Material Design concepts.</p>
            <p>It is a final result of ReactJS Essential Course.</p>
        </Paper>
    </div>
);

export default About;
